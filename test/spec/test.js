/**
 *
 * NIGHTMARE + MOCHA TEST
 *
 * Nightmare:
 * Permet de simuler des actions sur une page web
 * https://github.com/segmentio/nightmare
 *
 * mocha:
 * Permet de lancer des tests unitaires
 * https://mochajs.org/
 *
 * chai:
 * Librairie d'assertion (assertion library) qui permet d'effectuer
 * des tests avec les fonctions should() expect() et assert()
 * http://chaijs.com/
 *
 */

const Nightmare = require('nightmare');
const assert    = require('assert');
const chai      = require('chai');
const expect    = chai.expect;

// L'url de la page où éxécuter les tests
const BASE_URL  = 'http://localhost:9000';

// Définition du groupe de tests
describe('Check if mocha is working', function () {
  // Définition du test
  it('Check if true is not false', function () {
    // Test
    expect(true).not.to.equal(false);
  });

  it('Check the length of "foo"', function () {
    let foo = 'bar';

    expect(foo).to.have.lengthOf(3);
  });
});



describe('Try to load a Page', function () {

  // Délai avant l'exécution du test (chargement de la page)
  this.timeout('8s');

  let nightmare = null;

  // Fonction de mocha qui permet d'effectuer une fonction avant d'éxécuter les tests
  beforeEach(() => {
    //Init nightmare
    nightmare = new Nightmare({
      // Définir sur true pour ouvrir le test dans une nouvelle fenêtre
      show: true
    });
  });



  describe('Testing the home page', function () {

    it('should load without error', function (done) {
      nightmare
        // Ouvre la page
        .goto(BASE_URL)
        // Déconnecte nightmare de la page pour nettoyer le buffer
        .end()
        // Callback de succès: done() est appelé pour dire à mocha que le test s'est bien déroulé
        .then(() => done())
        .catch(done);
    });

    it('Check if the selector exists', function(done) {
      nightmare
        .goto(BASE_URL)
        // Détermine si le titre de la page existe
        .exists('h1.title')
        // Callback de la fonction exists() (boolean)
        .end()
        .then((exists) => {
          expect(exists).to.equal(true);
        })
        // Done
        .then(() => done())
        .catch(done);
    });

    it('Check if the response is "JAVA !"', function (done) {
      // L'élément HTML qui contient la réponse attendue
      let selector = '#response';

      // Modification du timeout de mocha pour laisser le temps à la réponse d'apparaître
      this.timeout(10000);

      nightmare
        .goto(BASE_URL)
        .click('#button')
        .wait(8000)
        .evaluate((selector) => {
          // Nous sommes maintenant dans le DOM
          return document.querySelector(selector).innerText;
        }, selector) // <-- selector est passé en paramètre depuis le scope de Node vers le scope du navigateur
        .end()
        .then((response) => {
          expect(response).to.equal('JAVA !');
        })
        .then(() => done())
        .catch(done);
    })
  })
});
