$(document).ready(() => {
  'use strict';

  const $container  = $('#response');

  let getAnswer = (cb) => {
    let dotes       = '',
        interval    = 500,
        duration    = 7,
        loop        = (duration * 1000) / interval,
        run         = 0;


    let doteInterval = setInterval(() => {
      dotes = dotes.length === 3 ? '' : dotes + '.';
      $container.html(dotes);
      run += 1;
      if (run >= loop) {
        clearInterval(doteInterval);
        cb('JAVA !');
      }
    }, interval);

  };

  $('#button').click(() => {
    getAnswer((answer) => {
      $container.html(answer);
    });
  });
});