# Petit test de Mocha et Nightmare


Example basique de tests unitaires réalisables avec MochaJs et NightmareJs.
Le projet est basé sur un boilerplate généré par yeoman (webapp)  => Désolé pour les quelques dépendances inutiles


## Getting Started


Une fois le projet cloné, installez les dépendances avec la commande:


```
npm i
```

Il faudra aussi installer Mocha globalement:


```
npm i -g mocha
```


## Fonctionnement


Pour lancer le test, il suffit de taper la commande suivante:

```
gulp serve // Lancer le server local
npm test // Lancer les tests
```

Par la suite, il serait largement possible d'ajouter une tâche gulp pour exécuter les tests.


 -> Le fichier de test se situe dans test/spec

 -> Les tests sont réalisés sur la page app/index.html

 -> Un petit script se trouve dans app/scripts/main.js


 ## Évolutions


 -> Créer des tests unitaires basiques compatibles avec le starter

 -> Automatiser avant le déploiement du code sur le server (avec Travis?)

 Je vais essayer d'enrichir un peu cet exemple avec des cas plus concrets! :)
